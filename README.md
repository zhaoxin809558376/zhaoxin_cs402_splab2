# CS402-Systems Programming Lab2-C Programming Basics

## 1.Goal

Describe the structure of the code and how it worksn

## 2.C version

Currently, code should target C11, i.e.

## 3.Files

//C file

readfile.c	

//Head file

readfile.h

//input file

input.txt

## 4.Head file

### 4.1.Manifest Constant

//The numbers of the employees.

#define DBSIZE 1024  

//assume that no first nor last name is longer than 64 characters.

#define NAMESIZE 64

//Print the length of *.

#define WIDTH 40

//Print the length of -.

#define WIDTH2 43

### 4.2.Define a struct

struct person
{
    int id;
    char name[MAXNAME];
    char lname[MAXNAME];
    int salary;
};

### 4.3.Define function

// compare function

int Com (const void *a, const void *b)

// A function that prints a certain number of *

void starbar(void)

// Functions to read values

int read_int(int *address)

// Print the Employees Database

void print_DB(struct employees employee[DBSIZE],int num)

// Print the employee info for the key position in the struction array

void print_by_key(struct employees employee[DBSIZE],int num)

// delete the employee for the key position in the struction array

void delete_by_key(struct employees *ptr,int num,int key)

// get largest salary employee's key position in the struction array

int get_largest_salary_index(struct employees employee[DBSIZE],int num)

// Functions of binary search

int binary_search(struct employees employee[DBSIZE],int l,int r,int x)

int binary_search_list(const int arr[],int l,int r,int x)

// lookup the employee info by ID

void lookup_by_ID(struct employees employee[DBSIZE],int num,int employee_id)

// update the employee info by key

void update_by_key(struct employees *ptr,int key,char first_name[],char last_name[],int salary)

// search for traversal

int search_lastname(struct employees employee[DBSIZE],int num,char surname[])

// lookup the employee by last name

void lookup_by_lastname(struct employees employee[DBSIZE],int num,char lname[])

// remove an employee

void remove_an_employee(struct employees employee[DBSIZE],int num,int employee_id)

// add an employee

void add_an_employee(struct employees employee[DBSIZE],int num,int con,first_name,last_name,int s)

// update an employee's info

void update_employee_info(struct employees employee[DBSIZE],int num,int employee_id)

// print the top X employees with the highest salaries

void print_M_employees(struct employees employee[DBSIZE],int num,int M)

// all the same last name employee

void all_last_name_employee(struct employees employee[DBSIZE],int num,char lname[])

//define the menu

char *menu[]={...}

// show the menu

void showMenu(void)

## 5.C file

### 5.1.Include head file

#include <stdlib.h>

#include <stdio.h>

#include <string.h>

#include <unistd.h>

#include "readfile.h"

### 5.2.Main

// read data form txt

if(open_file(fname) == -1)

{

        printf("error reading file.\n");
        return -1;
}

        FILE *fp = fopen(fname, "r");
        while(!feof(fp)
{

        fscanf(fp, "%d %s %s %d\n", &employee[n].id, &employee[n].name, &employee[n].lname, &employee[n].salary);
        array[n] = employee[n].id;
        n++;
}

close_file(fp);

// sort the employees

qsort(employee, 14, sizeof(struct person), Com);

// create menu

while(1)

{

        switch(option)
        {
                case 1: ... break;// print the Database.
                case 2: ... break;// Lookup by ID.
                case 3: ... break;// Lookup by Last Name.
                case 4: ... break;// Add an Employee.
                case 5: ... break;// Remove an employee.
                case 6: ... break;// Update an employee's information.
                case 7: ... break;// Print the X employees with the highest salaries.
                case 8: ... break;// Find all employees with matching last name.
                case 9: ... break;// Store the data and Quit. 
        }
}

## 6.Execute

Your program will take one command line argument, which is the name of the input file containing employee information. 
For example, telling your program to load the employee data stored in the file "small.txt" would look like:

./readfile input.txt

The menu will print on the terminal,look like:

****************************************

Employee DB Menu:

(1) Print the Database

(2) Lookup by ID

(3) Lookup by Last Name

(4) Add an Employee

(5) Remove an employee

(6) Update an employee's information

(7) Print the M employees with the highest salaries

(8) Find all employees with matching last name

(9) Quit

****************************************

Enter your option:

****************************************

// input 1

It will show the employees list.

// input 2

It will search for employees by ID,and print the employee an the terminal

// input 3

It will search for employees by lastname,and print the employee an the terminal

// input 4

Add new employee information at the end of the database.

// input 5

Remove an employee information from the database.

// input 6

Updata an employee's information

// input 7

Print the top X employees with the highest salaries

// input 8

Find all employees with the same last name

// input 9

Store the data and quit.