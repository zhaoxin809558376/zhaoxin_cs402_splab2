#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "readfile.h"

int main(int argc,char *argv[])
{
    if(argc<2)
    {
        printf("pass filename to read from .. \n");
        return 0;
    }
    char *fname = argv[1];
    
    //begin
    char first_name[NAMESIZE], last_name[NAMESIZE];
    struct employees employee[DBSIZE];
    int option;
    int emp_id, salary, confirm,X;
    int n = 0;
    
    // read file
     if (open_file(fname) == -1)
     {
         printf("error reading file.\n");
         return -1;
    }
    FILE *fp = fopen(fname, "r");
    while(!feof(fp))
    {
        fscanf(fp, "%d %s %s %d\n", &employee[n].id, &employee[n].first_name, &employee[n].last_name, &employee[n].salary);
        //array[n] = employee[n].id;
        n++;
    }
    fclose(fp);

    //sort
    qsort(employee, n, sizeof(struct employees), Com);
    
    // menu
    while(1)
    {
        //showMenu();
        starbar();
        showMenu();
        starbar();
        printf("Enter your option:\n");
        starbar();
        read_int(&option);
        starbar();
        switch(option)
        {
            case 1: //Print the Database
                print_DB(employee,n);
                Sleep(1000);
                break;
            case 2: //Lookup by ID
                printf("Enter a 6 digit employee id: \n");
                read_int(&emp_id);
                lookup_by_ID(employee,n,emp_id);
                Sleep(1000);
                break; 
            case 3: //Lookup by Last Name
                printf("Enter employee's last name(no extra spaces): \n");
                read_string(&last_name);
                lookup_by_lastname(employee, n, last_name);
                Sleep(1000);
                break; 
            case 4: //Add an Employee
                printf("Enter the first name of the employee:\n");
                read_string(&first_name);
                //scanf("%s",&name);
                printf("Enter the last name of the employee:\n");
                read_string(&last_name);
                //scanf("%s",&lname);
                printf("Enter the salary of the employee (between $30,000 and $150,000):\n");
                read_int(&salary);
                //scanf("%d", &salary);
                printf("Do you wanna to add the following employee to the DB?\n");
                printf("Enter 1 for yes,0 for no\n");
                read_int(&confirm);
                add_an_employee(employee, n, confirm,first_name,last_name,salary);
                n++;
                Sleep(1000);
                break;
            case 5: //Remove an employee

                printf("Warning! You are gonna to delete an employee!\n");
                printf("Enter a 6 digit employee id:\n");
                read_int(&emp_id);
                remove_an_employee(employee, n, emp_id);
                n--;
                Sleep(1000);
                break;
            case 6: //Update an employee's information
                printf("Enter a 6 digit employee id: \n");
                scanf("%d", &emp_id);
                update_employee_info(employee,n,emp_id);
                Sleep(1000);
                break;
            case 7: //Print the M employees with the highest salaries

                printf("Print the top X employees with the highest salaries \n");
                scanf("%d", &X);
                print_M_employees(employee, n, X);
                Sleep(1000);
                break;
            case 8: //Find all employees with matching last name
                printf("Enter the last name of the employees: \n");
                scanf("%s", last_name);
                all_last_name_employee(employee, n, last_name);
                Sleep(500);
                break;
            case 9: //Quit
                printf("Do you wanna store the new data into the file?\n");
                printf("Enter 1 for yes,0 for no.\n");
                scanf("%d", &confirm);
                if (confirm == 1)
                {
                    FILE *fp = fopen(fname, "w");
                    for (int i = 0; i < n;i++)
                    {
                        fprintf(fp, "%d %s %s %d\n", employee[i].id, employee[i].first_name, employee[i].last_name, employee[i].salary);
                    }
                    fclose(fp);
                }
                printf("Goodbye!!!");
                return 1;
        }
    }
}