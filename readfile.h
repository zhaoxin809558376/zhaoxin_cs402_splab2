#define NAMESIZE 64
#define DBSIZE 1024
#define WIDTH1 40
#define WIDTH2 43

// the struct of employees
struct employees
{
    int id;
    char first_name[NAMESIZE];
    char last_name[NAMESIZE];
    int salary;
};

// print numbers of *,-
void starbar(void)//
{
    int count;
    for (count = 1; count <= WIDTH1; count++)
    putchar('*');
    putchar('\n');
}
void starbar2(void)//
{
    int count2;
    for (count2 = 1; count2 <= WIDTH2; count2++)
    putchar('-');
    putchar('\n');
}

//print the Database
void print_DB(struct employees employee[DBSIZE],int num)
{
    starbar2();
    printf("Name                     SALARY     ID     \n");
    starbar2();
    for (int i = 0; i < num ; i++)
    {
        printf("%-10s %-10s %10d %10d\n", &employee[i].first_name, &employee[i].last_name, employee[i].salary, employee[i].id);
    }
    starbar2();
};

// print by key
void print_by_key(struct employees employee[DBSIZE],int num)
{
    //Given structure and id,this function prints the employee record if it exists
    starbar2();
    printf("Name                       SALARY    ID     \n");
    printf("%-10s %-10s %10d %10d\n", employee[num].first_name, employee[num].last_name, employee[num].salary, employee[num].id);
    starbar2();
}

// delete by key
void delete_by_key(struct employees *ptr,int num,int key)
{
    /*Given structure and id,this function deletes the employee record*/
    struct employees employee[DBSIZE];
    // Create a structure with value as original structure
    for (int i = 0; i < num;i++)
    {
        strcpy(employee[i].first_name, ptr->first_name);
        strcpy(employee[i].last_name, ptr->last_name);
        employee[i].salary = ptr->salary;
        employee[i].id = ptr->id;
        ptr++;
    }
    for (int i = 0; i < num;i++)
    {
        ptr--;
    }
    for (int c = 0; c < num;c++)
    {
        if(c>=key)
        {
            int nextindex = c + 1;
            ptr->salary = employee[nextindex].salary;
            ptr->id = employee[nextindex].id;
            strcpy(ptr->first_name, employee[nextindex].first_name);
            strcpy(ptr->last_name, employee[nextindex].last_name);
        }
        ptr++;
    }
    for (int i = 0; i < num;i++)
    {
        ptr--;
    }
}

//get largest salary employee index
int get_largest_salary_index(struct employees employee[DBSIZE],int num)
{
    int largest = 0;
    for(int i = 1; i < num; i++)
        if (employee[i].salary > employee[largest].salary)
            largest = i;
    return largest;

}

// binary_search
int binary_search(struct employees employee[DBSIZE],int l,int r,int x){
    int arr[DBSIZE];
    for (int i = 0; i < r; i++)
    {
        arr[i] = employee[i].id;
    }
    int key = binary_search_list(arr, l, r, x);
    return key;
}
int binary_search_list(const int arr[],int l,int r,int x)
{
    if(r>=l)
    {
        int mid = l + (r - l) / 2;
        /* if the element is present at the middle
           itself*/ 
        if(arr[mid] == x)
            return mid;
        /* if element is smaller than mid ,then
           it can only be present in left subarray*/
        if(arr[mid]>x)
            return binary_search_list(arr, l, mid - 1, x);
        /* else the element can only be present
           in right subarray*/
        return binary_search_list(arr, mid + 1, r, x);
    }
    /* we reach here when element is not
       present in array*/
    return -1;
}

//lookup by ID
void lookup_by_ID(struct employees employee[DBSIZE],int num,int employee_id)
{
    int key;
    key = binary_search(employee, 0, num, employee_id);
    if (key == -1)
    {
        printf("employee with id %d not found. \n", employee_id);
    }
    else
    print_by_key(employee, key);
}

// update_by_key
void update_by_key(struct employees *ptr,int key,char first_name[],char last_name[],int salary)
{
    for (int c = 0; c < key;c++)
        ptr++;
    strcpy(ptr->first_name, first_name);
    strcpy(ptr->last_name, last_name);
    ptr->salary = salary;
    for (int c = 0; c < key;c++)
        ptr--;
}

// search for traversal
int search_lastname(struct employees employee[DBSIZE],int num,char surname[])
{
    for (int i = 0; i < num;i++)
    {
        if(strcmp(employee[i].last_name,surname) == 0)
        {
            return i;
        }
    }
    return -1;
}

//lookup_by_lastname
void lookup_by_lastname(struct employees employee[DBSIZE],int num,char lname[])
{
    int key;
    key = search_lastname(employee, num, lname);
    if (key == -1)
    {
        printf("Employee with last name %s not found in DB. \n", lname);
    }
    else
    print_by_key(employee, key);
}

// remove an employee
void remove_an_employee(struct employees employee[DBSIZE],int num,int employee_id)
{
    int confirm;
    int key;
    key = binary_search(employee, 0, num, employee_id);
    if(key == -1)
    {
        printf("Employee with id %d not found in DB\n",employee_id);
    } 
    else
    {
        print_by_key(employee, key);
        printf("Are you sure to delete the above employee?\nEnter 1 for yew,0 for no\n");
        scanf("%d", &confirm);
        if(confirm == 1)
        {
            delete_by_key(employee, num, key);
            printf("Sucessfully deleted!\n");
        }
    }
}

//add an employee
void add_an_employee(struct employees employee[DBSIZE],int num,int con,first_name,last_name,int s)
{
    int arr[DBSIZE];
    for (int i = 0; i < num; i++)
    {
        arr[i] = employee[i].id;
    }
    if (con == 1)
    {
        if ((s >= 30000) && (s <= 150000))
        {
            int cur_id = employee[num - 1].id;
            int new_id = cur_id + 1;
            strcpy(employee[num].first_name, first_name);
            strcpy(employee[num].last_name, last_name);
            employee[num].salary = s;
            employee[num].id = new_id;
            arr[num] = employee[num].id;
            num++;
        }
        else
        {
            printf("salary invalid.please enter another salary amount. \n");
        }
    }
    else
    {
        printf("dont wanna to add this person into DB.\n");
    }
}

//update an employee's info
void update_employee_info(struct employees employee[DBSIZE],int num,int employee_id)
{
    int key;
    char fname[NAMESIZE];
    char lname[NAMESIZE];
    int salary;
    int confirm;
    key = binary_search(employee, 0, num, employee_id);
    if(key == -1)
        printf("Employee with id %d not found in DB\n", employee_id);
    else
    {
        print_by_key(employee, key);
        printf("Enter the updated first name of the employee: \n");
        scanf("%s",fname);
        printf("Enter the updated last name of the employee: \n");
        scanf("%s", lname);
        printf("Enter the updated employee's salary: \n");
        scanf("%s", salary);
        printf("Enter the updated first name of the employee: \n");
        printf("Do you wanna to update the folowing employee info to the DB?\n");
        printf("Enter 1 for yes,0 for no\n");
        scanf("%d", &confirm);
        if(confirm == 1)
        {
            update_by_key(employee, key, fname, lname, salary);
            printf("Sucessfully update! The updated info is as followed:\n");
            print_by_key(employee, key);
        }
        else
        {
            printf("The above employee was not update.\n");
        }
    }
}

//print the M employees with the highest salaries
void print_M_employees(struct employees employee[DBSIZE],int num,int M)
{
    // create a temp structure to look up and anther to store
    struct employees temp[DBSIZE], max_n[M];
    int temp_n = num;

    for (int i = 0; i < num;i++)
    {
        strcpy(temp[i].first_name, employee[i].first_name);
        strcpy(temp[i].last_name, employee[i].last_name);
        temp[i].salary = employee[i].salary;
        temp[i].id = employee[i].id;
    }
    for (int i = 0; i < M;i++)
    {
        int largest = get_largest_salary_index(temp, temp_n);
        // update the values in max_n;
        strcpy(max_n[i].first_name, temp[largest].first_name);
        strcpy(max_n[i].last_name, temp[largest].last_name);
        max_n[i].salary = temp[largest].salary;
        max_n[i].id = temp[largest].id;

        delete_by_key(temp, temp_n, largest);
        temp_n--;
    }
    print_DB(max_n, M);
}

// all last name employee
void all_last_name_employee(struct employees employee[DBSIZE],int num,char lname[])
{
    int flag = search_lastname(employee, num, lname);
    if(flag == -1)
    {
        printf("Employee with last name %s not found in DB\n", lname);
    }
    else
    {
        starbar2();
        printf("Name                       SALARY    ID     \n");
        starbar2();
        for (int i = 0; i < num;i++)
            if(strcasecmp(employee[i].last_name,lname) == 0)
                printf("%-10s %-10s %10d %10d\n", employee[i].first_name, employee[i].last_name, employee[i].salary, employee[i].id);
        starbar2();
    }
}

// define the menu
char *menu[] =
{
        "(1) Print the Database",
        "(2) Lookup by ID",
        "(3) Lookup by Last Name",
        "(4) Add an Employee",
        "(5) Remove an employee",
        "(6) Update an employee's information",
        "(7) Print the M employees with the highest salaries",
        "(8) Find all employees with matching last name",
        "(9) Quit",
};

// show the menu
void showMenu(void)
{
    printf("Employee DB Menu:\n");
    for (int i = 0; i < 9;i++)
    {
        printf(menu[i]);
        printf("\n");
    }
}

int open_file(char *fname)
{
    if(fopen(fname,"r") == NULL)
        return -1;
    return 0;
}

// input data;
int read_int(int *address)
{
    if(scanf("%d",address) == 0)
        return -1;
    return 0;
}
int read_float(float *address)
{
    if(scanf("%f",address) == 0)
        return -1;
    return 0;
}
int read_string(char *address)
{
    if(scanf("%s",address) == 0)
        return -1;
    return 0;
}
void close_file(FILE *fname)
{
    fclose(fname);
}
// compare to
int Com (const void *a, const void *b)
{
        struct employees *c = (struct employees *) a;
        struct employees *d = (struct employees *) b;
        return (*c).id - (*d).id;
}